# pi4-pihole

Pihole deployment on p4 with k3s

## Before deployment
 you will need to change the host names in pi-hole-ingress.yaml to reflect your local network

## k3s install on Pi4
ssh to your PI4 and sudo to root
intall k3s
```
curl -sfL https://get.k3s.io | sh -
```
watch for all the pods to start up
```
watch kubectl get pods -A
```
clone the git repo
```
git clone https://gitlab.com/onemanandafewelectrons/pi4-pihole.git
cd pi4-pihole
```
edit the pi-hole-ingress.yaml and set host names to reflect your network

install PiHole
```
kubectl create namespace pihole
kubectl apply -f .
```
set the admin password
```
kubectl exec -ti -n pihole `kubectl get pods -n pihole |grep ^pihole |awk '{print $1}'` -- pihole -a -p
```
find the ingress ip and add it to you local hostfile or as a dns server on your host ( it is the trafik service and should be the same ip as your node )
```
kubectl get svc 
```
## Uninstall
everything is contained in its own name space and to uninstall just remove the namespace
```
kubectl delete ns pihole
```
